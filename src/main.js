import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import * as Echarts from 'echarts'
import VueEcharts from 'vue-echarts'
import './style/index.css'
Vue.config.productionTip = false
Vue.prototype.$echarts = Echarts

Vue.component('v-echarts', VueEcharts)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
